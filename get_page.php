
<input id='url' type='text' placeholder='Your Domain'>

<div class='add-term'>
	<input type='text' id='term_name' placeholder='enter a search term'>
	<button type="button" onClick='add_term()'>add term!</button> 
	<button type="button" onClick='do_group_search()'>Search for these terms</button>
	<div id="warning" style="background-color: #89bcdca6; display: none" ></div>
</div>

<div class='search-box'>
	<table >
		<tbody id='results-table'></tbody>
	</table>
</div>

<div id='show_example' class='show_example'>
	<h2 id='page_title'>Your Page - Result Preveiw</h2>
	<h4 id='page_url'>www.yourResultHere.preview</h4>
	<p id='page_description'>a preview of your google search result will show here, try editing it below!</p>
</div>

<div id='test_changes' class='test_changes'>
	<input type='text' id='title_change' placeholder='try changing the preview title'>
	<input type='text' id='subtitle_change' placeholder='try changing the preview sub-title'>
	<input type='text' id='desc_change' placeholder='Edit the meta-description of your site'>
</div>

<style type="text/css">

	#page_title {
		color: #185ABC;
		font-family: arial,sans-serif;
	}
	#page_url {
		color: #0D652D;
	}

	#page_description {
		font-family: arial,sans-serif;
	}

	.show_example {
	    border: #000000a3;
	    border-style: solid;
	    border-radius: 2px;
	    border-width: 2px;
	    padding: 12px;
	}
	.test_changes input{
		width: 100%;
		color: grey;
	}

</style>

<script>

	var searches = []
	let searches_max_len = 10

	title_change.oninput = function() {
		input = document.getElementById('title_change');

		var fullTitle = document.getElementById('page_title').innerHTML 
		let title = input.value 

		//a space for formating
		title += ' '

		//itterates the rest of the string to insert the subtitle
		for (var i = fullTitle.search('-'); i < fullTitle.length; i++) {
			title += fullTitle[i];
		}
		
		//insert title into html 
		document.getElementById('page_title').innerHTML = title

  	};

	subtitle_change.oninput = function() {
		input = document.getElementById('subtitle_change');

		var fullTitle = document.getElementById('page_title').innerHTML 
		let subtitle = ''
		
		//itterates the begining of the string to insert the existing title
		for (var i = 0; i <= fullTitle.search('-'); i++) {
			subtitle += fullTitle[i];
		}

		//add a space for formating
		subtitle += ' '

		//add the new subtitle
		subtitle += input.value

		//insert full title into html
		document.getElementById('page_title').innerHTML = subtitle

  	};

  	desc_change.oninput = function() {
		input = document.getElementById('desc_change');
		if (input.value == '') {
    		document.getElementById('page_description').innerHTML = 'a preview of your google search result will show here, try editing it below!';
    	}
    	else {
    		document.getElementById('page_description').innerHTML = input.value;
    	}
  	};

	function check_valid_URL() {
		return true;
	}

	function do_group_search() {

		//block if no searches or too many searches
		if (searches.length == 0 || searches.length > searches_max_len) {
			send_warning('please add valid search terms to the list')
			return
		}

		for (var i = 0; i < searches.length; i++) {
			console.log(i);
			search_for_term(searches[i].term, searches[i].website)
		}
	}

	function add_term() {

		if (check_valid_URL() == false) {
			return;
		}

		let term = document.getElementById('term_name').value;
		//block if no website given
		if (document.getElementById('url').value == '') {
			send_warning('Please enter a valid URL to check.')
			return
		}
		//block if no term given
		if (term == '') {
			send_warning('please enter a search term')
			return
		}

		//block this term if allready added
		if (find_index_of_searches(term) != null) {
			//send a warning
			send_warning('You have allready added this term.')
			return
		}
		console.log(term)

		//blocks this term if too many terms
		if (searches.length >= searches_max_len) {
			//send a warning
			send_warning('There is a limit of 10 search terms.')
			return
		}

		// clear warnings from screen
		document.getElementById('warning').style.display = "none"

		var ellement = '<tr style="background-color: #89bcdca6" id=' + term.replace(/ /g, '_') + '><td>' + term + '</td><td id ="responce">...</td>'
		document.getElementById('results-table').innerHTML += ellement;
		let website = document.getElementById('url').value;

		searches.push({ term : term,
						website : website })
	} 

	function send_warning(warning) {
		console.log('sending warning')
		document.getElementById('warning').innerHTML = warning
		document.getElementById('warning').style.display = "block"
	}

	function search_for_term(term, website) {
		
		//send ajax request to server for to do a calculation of google search positioning
		//is this actually possable in drupal?
		//see page saved in favouites

		var xmlhttp = new XMLHttpRequest();
		var url = "module-test-result/" + term.replace(/ /g, '%20') + '/' + website;
		console.log(url);

		xmlhttp.onreadystatechange = function() {
			
		  	if (this.readyState == 4 && this.status == 200) {

			    try {
			    	var answer = JSON.parse(this.responseText);
				}
				catch {
			    	alert('there was an error! try re-phrasing??')
				}
				console.log('arr = ' + answer);
				
				//add to table
				let index = find_index_of_searches(answer['search-term'])
				searches[index].position = answer['position'];
				searches[index].page_title = answer['page_title'];
				searches[index].page_url = answer['page_url'];
				searches[index].page_description = answer['page_description'];

				//insert answer to html
				insert_answer(answer);

		  	}
		};

		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}

	function see_preview(term) {
		//loads the values of the search preview into the preview box

		let index = find_index_of_searches(term)

		if (searches[index]['page_title'] && searches[index]['page_url'] && searches[index]['page_description']) {
			document.getElementById('page_title').innerHTML = searches[index]['page_title']
			document.getElementById('page_url').innerHTML = searches[index]['page_url']
			document.getElementById('page_description').innerHTML = searches[index]['page_description']
		}
		else {
			console.log('reqeust for preview rejected');
		}

		//wipe values of the 3 text boxes
		document.getElementById('title_change').value = ''//'try changing the preview title'
		document.getElementById('subtitle_change').value = ''//'try changing the preview subtitle'
		document.getElementById().value = ''//'Edit the meta-description of your site'
	}

	function find_index_of_searches(term) {
		//a function to itterate through responces and return the desired index
		for (var i = 0; i < searches.length; i++) {
			if (searches[i]['term'] == term) {
				return i;
			} 
		}
		return null;
	}

	function insert_answer(answer) {
		console.log(answer);

		//here we need to find the table row where the de

		term = answer['search-term'] //create this if not created

		row = document.getElementById(term)

		//add to this row
		var ellement = '<td>' + term + '</td>';

		if (answer['position']) {
			preview_available = (answer['page_title'] && answer['page_url'] && answer['page_description'])
			ellement += '<td>your site is at position ' + answer['position'] + ' for this search term</td><td>' + ((preview_available) ? '<button class="table_button" onclick="see_preview(\'' + term + '\')">see preview</button>' : 'no preview available' + '</td></tr>');
		}
		else {
			ellement += '<td>Your website is not in the first page for this search term</td><td>No preview available</td></tr>';
		}
		document.getElementById(term.replace(/ /g, '_')).innerHTML = ellement;

		/*
		//paste this info onto the preview if there is info
		if (answer['page_title'] && answer['page_url'] &&answer['page_description']) {
			document.getElementById('page_title').innerHTML = answer['page_title']
			document.getElementById('page_url').innerHTML = answer['page_url']
			document.getElementById('page_description').innerHTML = answer['page_description']
		}
		*/

	}

</script>